package Aula08;

public abstract class FiguraGeometrica {
	
	public String nome;
	
	abstract double calcularArea();
	
	abstract double calcularPerimetro();

}
