import java.util.Scanner;

public class ExercicioCabuloso {
    public static void main(String[] args) {
        // Declaracao de variaveis e entrada de dados
        Scanner leitor = new Scanner(System.in);

        int bandejaCopos;
        int bandejaLatas;
        int totalCoposQuebrados = 0;

        do {
            System.out.print("Quantas latas tem na bandeja? ");
            bandejaLatas = leitor.nextInt();
            System.out.print("Quantos copos tem na bandeja? ");
            bandejaCopos = leitor.nextInt();
            if (bandejaLatas > bandejaCopos) {
                totalCoposQuebrados += CalcularCoposQuebrados(bandejaLatas, bandejaCopos);
            }

            System.out.print("O garçom levou mais bandejas? Insira S para adicionar outra bandeja: ");
        } while (leitor.next().equalsIgnoreCase("S"));

        leitor.close();

        // Saida de dados
        System.out.println("O total de copos quebrados é: " + totalCoposQuebrados);
    }

    public static int CalcularCoposQuebrados(int x, int y) {
        if (x > y) {
            return y;
        } else {
            return 0;
        }
    }
}
